package main

import (
	"github.com/labstack/echo"
	micro "github.com/micro/go-micro"
	d "gitlab.com/imkopkap/gateway-a/delivery/http"
	u "gitlab.com/imkopkap/gateway-a/usecase"
	proto "gitlab.com/imkopkap/service-a/delivery/grpc/proto"
)

func main() {
	service := micro.NewService(
		micro.Name("gateway"),
	)

	service.Init()

	e := echo.New()

	// Register Greeter Service
	greeter := proto.NewGreeterService("go.micro.srv.greeter", service.Client())

	// Register Usecase
	usecase := u.NewMyUsecase(greeter)
	d.NewMyDelievery(e, usecase)

	e.Logger.Fatal(e.Start(":1323"))
}
