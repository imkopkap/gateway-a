package delivery

import (
	"net/http"

	"github.com/labstack/echo"
	u "gitlab.com/imkopkap/gateway-a/usecase"
)

type Delievery interface {
	GetHello() string
}

type MyDelievery struct {
	usecase u.Usecase
}

func NewMyDelievery(e *echo.Echo, us u.Usecase) {
	handler := &MyDelievery{
		usecase: us,
	}

	e.GET("/", handler.GetHello)
}

func (d *MyDelievery) GetHello(c echo.Context) error {
	return c.JSON(http.StatusOK, d.usecase.GetHello())
}
