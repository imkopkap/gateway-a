package usecase

import (
	"context"
	"fmt"

	proto "gitlab.com/imkopkap/service-a/delivery/grpc/proto"
)

type Usecase interface {
	GetHello() string
}

type MyUsecase struct {
	greeter proto.GreeterService
}

func NewMyUsecase(greeter proto.GreeterService) Usecase {
	return &MyUsecase{
		greeter: greeter,
	}
}

func (u *MyUsecase) GetHello() string {
	rsp, err := u.greeter.GetHello(context.TODO(), &proto.Request{})

	if err != nil {
		fmt.Println(err)
	}

	return rsp.Greeting
}
