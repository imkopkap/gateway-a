module gitlab.com/imkopkap/gateway-a

go 1.13

require (
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/micro/go-micro v1.18.0
	gitlab.com/imkopkap/service-a v1.0.1
)
